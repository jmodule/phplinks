<?php
// *******************************************************************
//  index.php
// *******************************************************************

include("include/config.php");
include("include/functions.php");
include("include/common.php");

include("include/session.php");

// grab the values from the search form
//
if (isset($_GET['logic'])) {
    $logic = $_GET['logic'];
} else {
    if (isset($_POST['logic'])) {
        $logic = $_POST['logic'];
    }
}

if (isset($_GET['term'])) {
        $term = $_GET['term'];
} else {
    if (isset($_POST['term'])) {
        $term = $_POST['term'];
    }
}

if (isset($_GET['sr'])){ $sr = $_GET['sr']; }
if (isset($_GET['pp'])){ $pp = $_GET['pp']; }
if (isset($_GET['cp'])){ $cp = $_GET['cp']; }

$language = $gl["Language"];

include("include/lang/$language.php");

$theme = $gl["Theme"];

include("themes/$theme/tables.php");

$style = "netscape";

if(strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")){
	$style = "style";
}

// Check the form for categories to show
//
$show = null;

if (isset($_GET['show'])) {
    $show = $_GET['show'];
} else {
    if (isset($_POST['show'])) {
        $show = $_POST['show'];
    }
}

$PID = null;

if (isset($_GET['PID'])) {
    $PID = $_GET['PID'];
} else {
    if (isset($_POST['PID'])) {
        $PID = $_POST['PID'];
    }
}

// Parse the path for additional pages to show
//
//if(isset($_SERVER['PATH_INFO'])){
//	$url_array = explode("/",$_SERVER['PATH_INFO']);
//	if ($url_array[1] == "new.html"){$show="new";}
//	if ($url_array[1] == "popular.html"){$show="pop";}
//	if ($url_array[1] == "tags.html"){$show="tags";}
//	if ($url_array[1] == "cool.html"){$show="cool";}
//	if ($url_array[1] == "add.html"){$show="add";}
//	if ($url_array[1] == "about.html"){$show="about";}
//	if ($url_array[1] == "bookmarklet.html"){$show="bookmarklet";}
//	if ($url_array[1] == "review.html"){$show="review";}
//	if ($url_array[1] == "update.html"){$show="update";}
//	if ($url_array[1] == "owner.html"){$show="owner";}
//	if ($url_array[1] == "review_add.html"){$show="review_add";}
//	if (is_numeric (preg_replace ("/\.html/i", '', $url_array[1]))){
//		$PID = (preg_replace ("/\.html/i", '', $url_array[1]));
//		}
//}

?>

<!DOCTYPE html>
<html>
<head>
	<meta
		name    =   "keywords"
		content =   "phplinks,php,mysql,free,links,search engine,
					index,search index,keyword,results,meta data,
					script"
	/>
	<meta
		name    =   "description"
		content =   "phplinks is a free php script for use with 
					mysql. NewPHPLinks is a search engine, link farm
					script. NewPHPLinks is free for download."
	/>
	<meta
		name    =   "robots"
		content =   "all"
	/>
	<!--
		phplinks is a free php script for use with mysql.  phplinks
		is a search engine or link farm script. phplinks is free for
		download php script.
	-->
	<!--
		phplinks,php,mysql,free,links,search engine,index,search
		index,keyword,results,meta data,script
	-->
	<title>
		<?=$gl['SiteTitle']?>
	</title>
	<link
		rel     =   "stylesheet"
		type    =   "text/css"
		href    =   "themes/<?=$theme?>/<?=$style?>.css"
	/>
</head>

<body bgcolor="white"><?php

include("themes/$theme/header.php");
include("themes/$theme/navbar.top.php");

if(isset($term)){
	
	include("include/results.php");
	include("include/related.php");
}

if(isset($show)){
	
	if($show == "new" || $show == "pop" || $show == "cool"){   
		
		include("include/show.php"); 
	} else {     
		
		include("include/$show.php");
	}
}

if(!isset($term) and !isset($show)){
	
	if(!isset($PID)){   
		
		$PID=0;
		include("include/main.cats.php");
	} else {
		
		include("include/line.cat.php");
		include("include/main.cats.php");
		include("include/sites.php");
		include("include/related.php");
	}
}

include("themes/$theme/navbar.bottom.php");
include("themes/$theme/footer.php");

?>

</body>
</html>

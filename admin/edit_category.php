<?php
// *******************************************************************
//  admin/edit_category.php
// *******************************************************************

include("../include/config.php");
include("../include/functions.php");
include("../include/lang/$language.php");

include("../include/session.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
<head>
<title></title>
<link rel = "stylesheet" type = "text/css" href = "style.css" />
</head>
<body>
<?php
if(isset($_REQUEST['submit'])){
    $Category = str_replace(" ", "_", $_POST['Category']);
    $Children = $_POST['Children'];
    $TopChildren = $_POST['TopChildren'];
    $AllowSites = $_POST['AllowSites'];
    $ShowSiteCount = $_POST['ShowSiteCount'];
    $Description = $_POST['Description'];

    $update = sql_query("
			update
				$tb_categories
			set
				Category                = '{$Category}',
				Description             = '{$Description}',
				Children				= '{$Children}',
				TopChildren             = '{$TopChildren}',
				AllowSites              = '{$AllowSites}',
				ShowSiteCount           = '{$ShowSiteCount}'
			where
				ID='{$_REQUEST['ID']}'
		");
}
if(isset($_REQUEST['ID'])) {
    $get_category = sql_query("select * from $tb_categories where ID='{$_REQUEST['ID']}'");
    $get_row = sql_fetch_array($get_category);
    $category_name = str_replace("_", " ", $get_row['Category']);
    ?>
    <form method="post" action="edit_category.php?<?=session_name()?>=<?=session_id()?>">
    <input type="hidden" name="ID" value="<?=$_REQUEST['ID']?>">
	<table cellspacing="0" cellpadding="5" border="1" align="center" width="100%">
        <tr>
            <td colspan="2" align="center" nowrap="nowrap" class="theader">Edit Category <?= $category_name ?><?php if (isset($_REQUEST['submit'])){ ?> - Category has been updated<?php }?></td>
		</tr>
		<tr bgcolor="<?=$cell_color?>">
			<td nowrap="nowrap" class="text"> <label for="f_category">Category Name:</label> </td>
			<td>
				<input class="small" type="text" name="Category" id="f_category" size="25" value="<?= $category_name ?>"></td>
		</tr>
		<tr bgcolor="<?=$cell_color?>">
			<td class="text"><label for="f_description">Description:</label><br /><br />Hint: If you want to hand pick your sub categories, create the nescessary html and enter it for the description.  Then pick "Desc" as your Children Categories Option below.</td>
			<td><textarea class="small" name="Description" id="f_description" rows="5" cols="30"><?=$get_row['Description']?></textarea></td>
		</tr>
		<tr bgcolor="<?=$cell_color?>">
			<td class="text"><label for="f_children">Children Categories Options:</label></td>
			<td><select class="small" name="Children" id="f_children"><?php
			echo "<option value=\"Top\"";
			if($get_row['Children'] == "Top"){echo " selected";}
			echo ">Categories with Most Sites (Top)</option><option value=\"Rand\"";
			if($get_row['Children'] == "Rand"){echo " selected";}
			echo ">Random Children Categories (Rand)</option><option value=\"Desc\"";
			if($get_row['Children'] == "Desc"){echo " selected";}
			echo ">Description of Category (Desc)</option><option value=\"Vert\"";
			if($get_row['Children'] == "Vert"){echo " selected";} 
			echo ">List Children Categories Vertically (Vert)</option>"; 
			?></select></td>
		</tr>
		<tr bgcolor="<?=$cell_color?>">
			<td class="text"><label for="f_top_children">Viewable Children Categories:</label></td>
			<td><input size="3" class="small" type="text" name="TopChildren" id="f_top_children" value="<?=$get_row['TopChildren']?>" /></td>
		</tr>
		<tr bgcolor="<?=$cell_color?>">
			<td class="text"><label for="f_allow_sites">Allow Sites to populate this Category:</label></td>
			<td><select class="small" name="AllowSites" id="f_allow_sites"><?php
				echo "<option value=\"Y\"";
				if($get_row["AllowSites"] == "Y"){echo " selected";}
				echo ">Yes</option><option value=\"N\"";
				if($get_row["AllowSites"] == "N"){echo " selected";}
				echo ">No</option>";
			?></select></td>
		</tr>
		<tr bgcolor="<?=$cell_color?>">
			<td class="text"><label for="f_show_site_count">Show Site Count for this Category:</label></td>
			<td><select class="small" name="ShowSiteCount" id="f_show_site_count"><?php
				echo "<option value=\"Y\"";
				if($get_row["ShowSiteCount"] == "Y"){echo " selected";}
				echo ">Yes</option><option value=\"N\"";
				if($get_row["ShowSiteCount"] == "N"){echo " selected";}
				echo ">No</option>";
			?></select></td>
		</tr>
		<tr bgcolor="<?=$cell_color?>">
			<td align="center" colspan="2">
			<input class="button" type="submit" value=" Update Category " name="submit">
			</td>
		</tr><?php
		
		if(isset($_REQUEST['submit'])){
			?>
				<tr>
					<td class="text" align="center" colspan="2"><a 
					href="categories_main.php?<?=session_name()?>=<?=session_id()?>">Click here to edit other categories.</a></td>
				</tr><?php
		} ?>
    </table></form>
    <?php
} else {
?>
<form method="post" action="edit_category.php?<?=session_name()?>=<?=session_id()?>">
    <table cellspacing="0" cellpadding="5" border="1" align="center" width="100%">
        <tr>
            <td class="theader" align="center">
                Enter Category ID: <input class="small" type="text" name="ID" size="3">
            </td>
        </tr>
        <tr>
            <td align="center">
                <input class="button" type="submit" value=" Edit Category " name="edit_category">
            </td>
        </tr>
    </table>
</form>
<?php
}
?>
</body>
</html>

<?php
// *******************************************************************
//  admin/index.php
// *******************************************************************

include("../include/config.php");
include("../include/functions.php");
include("../include/common.php");
$language = $gl["Language"];
include("../include/lang/$language.php");
include("../include/session.php");
?>
<!DOCTYPE html>

<html>
	<head>
		<title>NewPHPLinks Admin</title>
	</head>
	<frameset cols="160,*">
		<frame name="menu" src="menu.php?<?=session_name()?>=<?=session_id()?>" scrolling="auto" marginwidth="10" marginheight="10" topmargin="10" leftmargin="10" />
		<frame name="main" src="main.php?<?=session_name()?>=<?=session_id()?>" scrolling="auto" marginwidth="10" marginheight="10" topmargin="10" leftmargin="10" />
	</frameset>
</html>

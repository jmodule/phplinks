<?php
// *******************************************************************
//  admin/purge.php
// *******************************************************************

include("../include/config.php");
include("../include/functions.php");

include("../include/common.php");
$language = $gl["Language"];

include("../include/lang/$language.php");

include("../include/session.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
<head>
<title></title>
<link rel = "stylesheet" type = "text/css" href = "style.css" />
</head>
<?=$adm_body?>
<table cellspacing="0" cellpadding="5" border="1" align="center" width="100%">
<tr>
	<td colspan="2" class="theader">Purge Temporary Sites Table</td>
</tr><?php

if(isset($_REQUEST['submit'])){

    if(isset($_REQUEST['purge_except']) && !isset($_REQUEST['purge_all'])) {

        $email = $_REQUEST['purge_email'];

        $result = sql_query("
            select
              ID
            from
              $tb_temp
            where
              Email != '$email'");

        $del_results = array();
        while($rows = sql_fetch_array($result)) {
            $sql = "
			delete from
				$tb_temp
			where
				ID='". $rows['ID'] . "'";
            $del_results[] = sql_query($sql);
        }

        $html .= "<tr><td class=\"text\">All submissions, except those from " . $_REQUEST['purge_email'] . " have been purged.</td></tr>";
        $html .= "<tr><td class=\"text\">Removed " . count($del_results) . " record(s) from the table.</td>";
    }
	elseif(isset($_REQUEST['purge_all']) && !isset($_REQUEST['purge_except'])) {
		
		$del_results = sql_query("
			delete from
				$tb_temp
		");

		$html .= "<tr><td class=\"text\">All pending submissions have been purged.</td></tr>";
        $html .= "<tr><td class=\"text\">Removed $del_results record(s) from the table.</td>";
	} elseif (!isset($_REQUEST['purge_all']) && !isset($_REQUEST['purge_except'])) {
        $html .= "<tr><td class=\"text\">Nothing has been purged.</td></tr>";
    } else {
        $html .= "<tr><td class=\"text\">You cannot select Purge All and Purge All Except at the same time.</td>";
    }

	echo $html;
} else {
	?><tr><form method="post" action="purge.php?<?=session_name()?>=<?=session_id()?>">
		<td width="75%" class="text">Purge All:<br />This delete all pending site submissions in the temporary table.</td>
		<td width="25%"><input class="small" type="checkbox" name="purge_all"></td>
	</tr>
	<tr>
		<td class="text">Purge All Except by:<br />This will all sites that were not submitted by the given email address.</td>
		<td>
            <input class="small" type="checkbox" name="purge_except">&nbsp;
            <input class="small" type="text" name="purge_email" size="35" value="<?=$prefill_email?>">
        </td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input class="button" type="submit" 
		name="submit" value="Reset Values"></td>
	</form></tr><?php
}
?>
</table>
</body>
</html>

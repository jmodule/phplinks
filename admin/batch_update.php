<?php
// *******************************************************************
//  admin/fix_passwords.php
// *******************************************************************

include("../include/config.php");
include("../include/functions.php");

include("../include/common.php");
$language = $gl["Language"];

include("../include/lang/$language.php");

include("../include/session.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html lang="en">
<head>
<title></title>
<link rel = "stylesheet" type = "text/css" href = "style.css" />
</head>
<body>
<?=$adm_body?>
    <table cellspacing="0" cellpadding="5" border="1" align="center" width="100%">
        <tr>
            <td colspan="2" class="theader">Batch Update Site Submissions</td>
        </tr>
        <?php
        $password = '';
        $confirm = '';

if(isset($_REQUEST['submit'])){

    if(isset($_REQUEST['update_password_do'])) {

        $username = $_REQUEST['update_username'];
        $password = $_REQUEST['update_password'];
        $confirm = $_REQUEST['update_password_confirm'];

        if ($password == $confirm) {

            $result = sql_query("
            select
              ID
            from
              $tb_links
            where
              UserName = '$username'");

            $update_results = array();
            while ($rows = sql_fetch_array($result)) {
                // Generate a new hash for each row
                $enc_passwd = get_password_hash($password);

                $sql = "
                update
                    {$tb_links}
                set
                    Password = '{$enc_passwd}'
			    where
				    ID='" . $rows['ID'] . "'";
                $update_results[] = sql_query($sql);
            }

            $html .= "<tr><td colspan=\"2\" class=\"text\">Updated the password for sites submitted by " . $_REQUEST['update_username'] . ".</td></tr>";
            $html .= "<tr><td colspan=\"2\" class=\"text\">Changed " . count($update_results) . " record(s).</td>";
        } else {
            $html .= "<tr><td colspan=\"2\" class=\"alert\">Passwords do not match, please try again.</td>";
        }
    } else {
        $html .= "<tr><td colspan=\"2\" class=\"alert\">You did not select a batch action to perform.</td>";
    }

	echo $html;
}
	?><form method="post" action="batch_update.php?<?=session_name()?>=<?=session_id()?>">
	<tr>
		<td class="text">Update all Passwords for:<br />This will update all sites that submitted by the given username.</td>
		<td>
            <input class="small" type="checkbox" name="update_password_do">&nbsp;
            <input class="small" type="text" name="update_username" size="35" value="<?=$prefill_username?>"><br/>
            <input class="small" type="password" name="update_password" size="35" placeholder="Password" value="<?=$password?>">
            <input class="small" type="password" name="update_password_confirm" size="35" placeholder="Confirm Password" value="<?=$confirm?>">
        </td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input class="button" type="submit" name="submit" value="Submit"></td>
	</tr>
    </form>
</table>
</body>
</html>

<?php
// *******************************************************************
//  admin/edit_site.php
// *******************************************************************

include("../include/config.php");
include("../include/functions.php");

include("../include/common.php");
$language = $gl["Language"];

include("../include/lang/$language.php");

include("../include/session.php");

$update = $_REQUEST['update'];
$ID = $_REQUEST['ID'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
<head>
<title></title>
<link rel = "stylesheet" type = "text/css" href = "style.css" />
    <script type="application/javascript">
        function toggleReadonly(obj, field_name) {
            document.getElementById(field_name).readOnly = !obj.checked;
        }
    </script>
</head>
<?=$adm_body?>
<?php
if(isset($update) && isset($ID)){

    $SiteName = $_REQUEST['SiteName'];
    $SiteURL = $_REQUEST['SiteURL'];
    $Description = $_REQUEST['Description'];
    $Category = $_REQUEST['Category'];
    $Country = $_REQUEST['Country'];
    $Email = $_REQUEST['Email'];
    $user_name = $_REQUEST['Username'];
    $plaintext = $_REQUEST['Password'];

	$result = sql_query("
		update
			$tb_links
		set
			SiteName='$SiteName',
			SiteURL='$SiteURL',
			Description='$Description',
			Category='$Category',
			Country='$Country',
			Email='$Email'
		where
			ID='$ID'
	");

    if (isset($_REQUEST['change_username'])) {
        $result2 = sql_query("
          UPDATE
            $tb_links
          SET
            Username='$user_name'
          WHERE
            ID='$ID'
        ");
    }

    if (isset($_REQUEST['change_password'])) {
        $password = get_password_hash($plaintext);
        $result3 = sql_query("
          UPDATE
            $tb_links
          SET
            Password='$password'
          WHERE
            ID='$ID'
        ");
    }
}

if(isset($ID)){

	$get_site = sql_query("
		select
			*
		from
			$tb_links
		where
			ID='$ID'
	");

	$rows = sql_fetch_array($get_site);
	
	?>
    <form method="post" action="edit_site.php?<?=session_name()?>=<?=session_id()?>">
	    <input type="hidden" name="ID" value="<?=$ID?>">
	    <?php if($p==1){ ?>
            <input type="hidden" name="p" value="1">
        <?php } ?>
        <table cellspacing="0" cellpadding="5" border="1" align="center" width="100%">
        <tr>
            <td colspan="2" class="theader">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td class="theader">Edit Site<?php
                        if(isset($update) && isset($ID)){
                            ?> - Site Has been updated.<?php
                        }
                        ?></td><?php
                        if($p==1){
                            ?><td align="right" class="text">[ <a
            href="<?=$rows['SiteURL']?>" target="_blank">View Site</a> ] [ <a
            href="delete_site.php?<?=session_name()?>=<?=session_id()?>&amp;dbtable=links&amp;ID=<?php

            echo $rows['ID'];

            if(isset($p)){
                echo "&amp;p=" . $p;
            }
            ?>">Delete Site</a> ] [ <a href="javascript:window.close();">Close Window</a> ]</td><?php
                        }?>
                </tr>
                </table>
                </td>
        </tr>
        <tr>
            <td class="text"><label for="f_SiteName">Site Name:</label></td>
            <td><input class="small" type="text" name="SiteName" id="f_SiteName" value="<?=stripslashes($rows['SiteName'])?>" size="35"></td>
        </tr>
        <tr>
            <td class="text"><label for="f_SiteURL">Site URL:</label></td>
            <td class="text"><input class="small" type="text" name="SiteURL" id="f_SiteURL" value="<?=$rows['SiteURL']?>" size="35"></td>
        </tr>
        <tr>
            <td class="text"><label for="f_Description">Description:</label></td>
            <td><textarea class="small" name="Description" id="f_Description" rows="7" cols="40" wrap="virtual"><?=stripslashes($rows['Description'])?></textarea></td>
        </tr>
        <tr>
            <td class="text">Category: </td>
            <td><select class="small" name="Category"><?php
                drop_cats($rows['Category'], 0, "", $cats);
                echo $cats;
            ?></select></td>
        </tr>
        <tr>
            <td class="text">Country: </td>
            <td><select class="small" name="Country"><?php
            if($d = dir("../images/flags")){
                echo getFlagList("../images/flags", $rows['Country']);
            }
            ?></select></td>
        </tr>
        <tr>
            <td class="text">Email</td>
            <td><input class="small" type="text" name="Email" value="<?=$rows['Email']?>" size="35"></td>
        </tr>
        <!-- Inquiry fields to show the user information for this entry -->
        <tr>
            <td class="text">Username</td>
            <td>
                <input type="checkbox" name="change_username" onclick="toggleReadonly(this, 'f_username')"> Change Username:&nbsp;
                <input class="small" type="text" id="f_username" name="Username" value="<?=$rows['UserName']?>" size="35" readonly>
            </td>
        </tr>
        <tr>
            <td class="text">Password</td>
            <td>
                <input type="checkbox" name="change_password" onclick="toggleReadonly(this, 'f_password')"> Change Password:&nbsp;
                <input class="small" type="text" id='f_password' name="Password" value="<?=$rows['Password']?>" size="35" readonly>
            </td>
        </tr>

        <tr>
            <td colspan="2" align="center"><input class="button" type="submit" name="update" value=" Update Site "></td>
        </tr></table>
    </form>
<?php
}

if(!isset($ID) && !isset($update)){
	?>
    <form method="post" action="edit_site.php?<?=session_name()?>=<?=session_id()?>">
    <table cellspacing="0" cellpadding="5" border="1" align="center" width="100%">
	<tr>
		<td class="theader"><label for="ID">Please enter a site ID:</label>
		<input class="small" type="text" id="ID" name="ID" size="5">
		<input class="button" type="submit" name="submit" value=" Edit Site "></td>
	</tr>
	</table>
    </form>
    <?php
}
?>
</body>
</html>

<?php
// add_site.php Version 0.92 pre release
// custom Dew-NewPHPLinks bookmarklet creator
// last edited on Date: 2005.12.06
// written by: M. Waldis europa_austria@hotmail.com
// contributers: none yet
// licence: open sourced: GNU General Public License
// What it does: Bookmarklet customizor and creator for Dew-NewPHPLinks
//
// CHANGES-History:
//  0.92 Added xhtml links to Dew-NewPHPLinks and Forum to navigation
//  0.92 simplyfied some styles for navigation-display
//	0.92 changed some targets of external links to _blank
// 
// Note: TESTING! lots of testing...
//  strip all sorts of dangerous things from userinput Prefix variables
//  There might be a problem with web-sites spamming the description or 
//  keyword fields with too many words/content -->> split after x chars 
//  to avoid the url to break (url-limit)
//  maybe integrate a version of it as a plugin

// PERMANENT SETTINGS (optional) to get values fixed (unchangable by user)
// if you set them here, they will (in future) not show up in the web interface-form
// to set up your bookmarklet!!
// The latter is not completely implemented yet - useful at all?

/// the variables below can be left blank, but would need to be typed
//  into the form in order to generate a bookmarketlet. If only you will
//  use this script, you should probbaly fill them in.

// Your instalation location. 
// Should look like this "http://www.yoursite.com/install_dir"
// NO trailing slash!
$v_phplinksurl = ""; 

$v_UserName = "";
$v_PW = "";

$v_PW2 = "";
$v_Hint = "";
$v_Email = "";

$v_prefix_key = "";      //  
$v_prefix_author = "";   //
$v_prefix_modified = ""; //

$v_width = "400"; // recommended: 480
$v_height = "520"; // recommended: 520
$v_scrollbars = "yes"; // recommended: yes
$v_resizable = "yes"; // recommended: yes

// don't change these!!! no need to anyway-
// default settings for necessary vars 
// anything not given usually defaults to no
// scrollbars=no,menubar=no,status=no,toolbar=no,resizable=no
$d_width = "480";
$d_height = "520";
$d_scrollbars = "yes";
$d_resizable = "yes";
// default settings for additional info:
$v_description=TRUE;
$v_keywords=TRUE;
$v_author=TRUE;
$v_lastModified=TRUE;
$d_phplinksurl = "http://www.yourserver.com/phplinksdirectory";
$d_prefix_key = ", // keywords-->> // ";
$d_prefix_author = ", // author-->> // ";
$d_prefix_modified = ", last modified: ";

// mode control/navigation
$r_mode = mysql_escape_string($_REQUEST['r_mode']);
$t_mode = $r_mode;//variable that sets what mode to display
if($_REQUEST['r_mode']==""||$_REQUEST['r_mode']==FALSE):$_REQUEST['r_mode']=0;$t_mode=0;endif;
if($t_mode==9){$t_mode2=4;}else{$t_mode2=$t_mode;}
$t_modeplus = "c_mode$t_mode2";
$defstate="hide";
$arr_c_mode =
array('c_mode0'=>'','c_mode1'=>'','c_mode2'=>'','c_mode3'=>'','c_mode4'=>'','c_mode5'=>'');
   
foreach($arr_c_mode as $k => $v){reset ($arr_c_mode);
if($k==$t_modeplus){$arr_c_mode["$k"]="show";}else{$arr_c_mode["$k"]="$defstate";}$i++;}
extract($arr_c_mode);

// getting all request vars from input array if no admin-default set
$t_mode = mysql_escape_string($_REQUEST['r_mode']);
if($v_phplinksurl=="")$v_phplinksurl = mysql_escape_string($_REQUEST['r_phplinksurl']);
if($v_width=="")$v_width = mysql_escape_string($_REQUEST['r_width']);
if($v_height=="")$v_height = mysql_escape_string($_REQUEST['r_height']);
if($v_scrollbars=="")$v_scrollbars = mysql_escape_string($_REQUEST['r_scrollbars']);
if($v_resizable=="")$v_resizable = mysql_escape_string($_REQUEST['r_resizable']);
if($v_UserName=="")$v_UserName = mysql_escape_string($_REQUEST['r_UserName']);
if($v_PW=="")$v_PW = mysql_escape_string($_REQUEST['r_PW']);
if($v_PW2=="")$v_PW2 = mysql_escape_string($_REQUEST['r_PW2']);
if($v_Hint=="")$v_Hint = mysql_escape_string($_REQUEST['r_Hint']);
if($v_Email=="")$v_Email = mysql_escape_string($_REQUEST['r_Email']);
$v_description = mysql_escape_string($_REQUEST['r_description']);
$v_keywords = mysql_escape_string($_REQUEST['r_keywords']);
$v_author = mysql_escape_string($_REQUEST['r_author']);
$v_lastModified = mysql_escape_string($_REQUEST['r_lastModified']);
if(mysql_escape_string($_REQUEST['r_prefix_key'])==""):$v_prefix_key=$d_prefix_key;else:$v_prefix_key
= mysql_escape_string($_REQUEST['r_prefix_key']);endif;
if(mysql_escape_string($_REQUEST['r_prefix_author'])==""):$v_prefix_author=$d_prefix_author;else:$v_prefix_author
= mysql_escape_string($_REQUEST['r_prefix_author']);endif;
if(mysql_escape_string($_REQUEST['r_prefix_modified'])==""):$v_prefix_modified=$d_prefix_modified;else:$v_prefix_modified
= mysql_escape_string($_REQUEST['r_prefix_modified']);endif;

function bkm_noie(){ //creates bookmarklet code for most of browsers
global $v_phplinksurl,$v_Email,$v_UserName,$v_PW,$v_PW2,$v_Hint,$v_Email;
global
$v_description,$v_keywords,$v_author,$v_lastModified,$v_width,$v_height,$v_scrollbars,$v_resizable;
global $d_width,$d_height,$d_scrollbars,$d_resizable;
global $v_prefix_key,$v_prefix_author,$v_prefix_modified;
// build bookmarklet for non-IE browsers
$bkm_1 = "javascript:void(d=document);";
$bkm_1 .= "function g2(mty){var mlm=d.all?d.all.tags('META'):d.getElementsByTagName?d.getElementsByTagName('META'):new Array();var mKy=new Array();var i=0;for(var m=0;m<mlm.length;m++)if(mlm[m].name==mty)mKy[i++]=mlm[m].content;return mKy;}function SE(n){return(((n=='')||((n+'')=='undefined')||(n+''=='null'||typeof(n)=='null'))?false:true);}";
// conditionals
if($v_description)$bkm_1.="x=g2('description');";
if($v_keywords)$bkm_1.="y=g2('keywords');";
if($v_author)$bkm_1.="w=g2('author');";
if($v_description||$v_keywords||$v_author||$v_lastModified)$bkm_1 .= "z=";
if($v_description)$bkm_1.="x"; // description
if($v_description||$v_keywords)$bkm_1.="+";
if($v_keywords)$bkm_1.="(SE(y)?'$v_prefix_key'+y :'')";
if($v_keywords||$v_author)$bkm_1.="+";
if($v_author)$bkm_1.="(SE(w)?'$v_prefix_author'+w:'')";
if($v_author||$v_lastModified)$bkm_1.="+";
if($v_lastModified)$bkm_1.="'$v_prefix_modified'+ window.d.lastModified";
if($v_description||$v_keywords||$v_author||$v_lastModified)$bkm_1.=";";
// this stays the same- more or less
$bkm_1 .= "rf='$v_phplinksurl";
$bkm_1 .= "/index.php?show=add&PID='";
$bkm_1 .= "+'&SiteName='+escape(d.title)+'&SiteURL='+escape(d.location.href)";
if($v_description||$v_keywords||$v_author||$v_lastModified)$bkm_1 .=
"+'&Description='+escape(z)";
$bkm_1 .= "+'&UserName=$v_UserName&PW=$v_PW&PW2=$v_PW2&Hint=$v_Hint&Email=$v_Email';";
$bkm_1 .= "void(open(rf,'LinkIt','";
if($v_width=="")$v_width=$d_width;
if($v_height=="")$v_height=$d_height;
if($v_scrollbars=="")$v_scrollbars=$d_scrollbars;
if($v_resizable=="")$v_resizable=$d_resizable;
$bkm_1 .= "width=$v_width,height=$v_height,";
$bkm_1 .= "scrollbars=$v_scrollbars,resizable=$v_resizable'));";
return $bkm_1;
}

function bkm_ie(){ // creates cut down IE version of bookmarklet
global $v_phplinksurl,$v_Email,$v_UserName,$v_PW,$v_PW2,$v_Hint,$v_Email;
global
$v_description,$v_keywords,$v_author,$v_lastModified,$v_width,$v_height,$v_scrollbars,$v_resizable;
global $d_width,$d_height,$d_scrollbars,$d_resizable;
// build bookmarklet for IE browsers
$bkm_2 = "javascript:void(d=document);";
$bkm_2 .= "function g(t){var l=d.all?d.all.tags('META'):d.getElementsByTagName?d.getElementsByTagName('META'):new Array();var K=new Array();var i=0;for(var m=0;m<l.length;m++)if(l[m].name==t)K[i++]=l[m].content;return K;}";
// conditionals
if($v_description||$v_keywords)$bkm_2 .= "z=";
if($v_description)$bkm_2.="g('description')";
if($v_description && $v_keywords)$bkm_2.="+', '+";
if($v_keywords)$bkm_2.="g('keywords')";
if($v_description||$v_keywords)$bkm_2 .= ";";
// this stays the same
$bkm_2 .= "rf='$v_phplinksurl";
$bkm_2 .= "/index.php?show=add&PID='";
$bkm_2 .= "+'&SiteName='+escape(d.title)+'&SiteURL='+escape(d.location.href)";
$bkm_2 .= "+'&Description='+escape(z)";
$bkm_2 .= "+'&UserName=$v_UserName&Email=$v_Email';";
$bkm_2 .= "void(open(rf,'t2',''));";
return $bkm_2;
}

function buildlinks(){ // builds the Bookmarklet-Links (short explanations)
global $r_mode,$v_phplinksurl,$t_mode,$d_phplinksurl;
if($t_mode==9 && ($v_phplinksurl !==$d_phplinksurl && $v_phplinksurl !=="")){
// build bookmarklet LINKS 
$bkm_link1 = '<a href="';
$bkm_link1 .= bkm_noie();
$bkm_link1 .= '">Dew-NewPHPLinks Bookmarklet (all Browsers except IE)</a>';
$bkm_link2 = '<a href="';
$bkm_link2 .= bkm_ie();
$bkm_link2 .= '">Dew-NewPHPLinks Bookmarklet (IE only)</a>';
// non IE info
$bkm_info1= "<br />ALL java script enabled &amp; capable browsers --NOT IE-- (~860
chars)<br />";
$bkm_info2= "<br />IE-only (~481 chars) reduced functionality<br />";
// link splitter for display
$bkm_split = '<hr align="left" size="5px" width="500px" />';
return ($bkm_split.$bkm_link1.$bkm_info1.$bkm_split.$bkm_link2.$bkm_info2.$bkm_split);
}
return('you have to properly customize your Bookmarklet first!<br />-->> <a
href="javascript:history.go(-1);">Please return to the customisation page</a>
<<--');
}
?>

<!doctype html public "-//w3c//dtd html 4.0 //en">
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
 <meta name="author" content="m. waldis europa_austriaAThotmailDOTcom">
 <title>Dew-NewPHPLinks Bookmarklet Creator Plugin v0.92</title>
 <meta name="description" content="Bookmarklet creator plugin for Dew-NewPHPLinks v0.92">
 <meta name="keywords" content="dew-newphplinks,phplinks,bookmarklet,bookmarks,link exchange,link list,database,online directory, mysql, php">
</head>
<style type="text/css">
    body {
        background: #B2D6F5;
        color: #444444;
        font-family: verdana, arial, sans-serif;
        font-size: 78%;
        padding-top: 5px;
        padding-left: 10%;
        padding-right: 10%;
        padding-bottom: 20px;
    }

    h1, h2 {
        font-family: verdana, arial, sans-serif;
        margin-bottom: 0;
    }

    h1 {
        font-size: 125%;
    }

    h2, h3, h4 {
        font-size: 110%;
    }

    a:link, a:active {
        font-size: 105%;
        color: #00306A;
    }

    a:hover {
        background-color: #347AB7;
        color: #FFFFFF;
        text-decoration: none;
    }

    a.visited {
        font-size: 115%;
        color: #694E93;
        text-decoration: none;
    }

    hr {
        background-color: #347AB7;
    }

    pre {
        padding: 0.5em;
        margin-left: 1em;
        margin-right: 2em;
        white-space: pre;
        background-color: #97B4E1;
        color: black;
    }

    .yourbookmarklet {
        background-color: red;
        padding: 10px;
    }

    #footer {
        margin: 60px 20% 10px;
        font-family: "American Typewriter", "Courier New", "Courier", monospace;
        font-size: 11px;
        color: #222;
        padding: 0.5em 1em;
        background-color: #97B4E1;
    }

    div {
        visibility: hidden;
        position: relative;
        float: left;
    }

    #frame {
        border: thin dotted #00046F;
        padding: 15px;
    }

    .hide {
        visibility: hidden;
        state: hidden;
        display: none;
    }

    .show {
        display: block;
        visibility: visible;
        state: show;
    }

    li {
        margin: 6px 6px 6px 6px;
    }

    input, select {
        margin-bottom: 3px;
        background-color: #F4F4F4;
    }

    .submit {
        padding: 5px;
        background-color: #347AB7;
        color: #FFFFFF;
    }

    /*   MAIN Navigation Styles   */
    #tabNavigation {
        border-bottom: 1px solid #000;
        text-align: left;
    }

    #tabNavigation li {
        display: inline;
        height: 200px;
        margin: 1px 1px 1px 0px;
        padding: 1px;
        z-index: 1000
    }

    #tabNavigation a, #tabNavigation a:link, #tabNavigation a:visited {
        background: #347AB7;
        color: #F4F4F4;
        padding: 2px 5px 2px 5px;
        text-decoration: none
    }

    #tabNavigation a:hover {
        background: #004E93;
        color: #F4F4F4
    }

    #tabNavigation a:active {
        background: #9F153E;
        color: #F4F4F4
    }
</style>

<body><div class="visible" id="frame">

<table width="100%"><tr><td>
<a href="http://www.dew-code.com/"  target="_blank"> Dew-Code.com</a> &nbsp;
<a href="http://www.dew-code.com/bookmarklet_092.php.gz">Download this file</a> &nbsp; 
<a href="http://www.dew-code.com/modules/newbb/viewtopic.php?viewmode=flat&topic_id=476&forum=15" target="_blank">Questions + Feedback Forum</a><br /><br />
<div class="visible" id="tabNavigation">
<a href="?r_mode=0" onclick="">0 Introduction</a>
<a href="?r_mode=1" onclick="">1 Preconditions</a>
<a href="?r_mode=2" onclick="">2 Prepare Dew-NewPHPLinks</a>
<a href="?r_mode=3" onclick="">3 Customize</a>
<a href="?r_mode=4" onclick="">4 The Bookmarklet</a>
<a href="?r_mode=5" onclick="">5 Use It!&nbsp;</a>
</div>
</td></tr></table>
<hr size="3px"/>
<div class="<?php echo $c_mode0 ?>">
<h1>0 &nbsp;Introduction</h1>
<h4>in brief</h4>
Bookmarklets are tools to help with repetitive or otherwise impossible tasks in your
web browser. It is like a normal bookmark but performs a certain, more complex task
or function. 
<br /><br />
This Bookmarklet is made to facilitate the entry of new urls into a <a
href="http://www.dew-code.com/modules/mydownloads/" target="self">Dew-NewPHPLinks</a>
link database management system. Just click the bookmark and it will fill out the
"new-link" form almost by itself using the data from the web-site you are currently
visiting.<br /><br />
Basic steps for bookmarklet usage:<br />
<strong><ol>
<li>Read through the above links step by step from 1 to 5 and follow the
guidelines</li>
<li>Drag the bookmarklet to your Bookmarks Toolbar or Links Bar.</li>
<li>While viewing a page you want to enter in your Dew-NewPHPLinks database, click the
bookmarklet from your Bookmarks Toolbar.</li>
</ol></strong>
<h4>in detail</h4>
This is an advanced bookmarklet for all of you that think it&#39;s a useless
pain/nusance to enter all the already existing data manually! Just click on it, and
an almost ready to submit Dew-NewPHPLinks "new link form" will show in a popup window.
<br />
This bookmarklet tries to catch all relevant meta-data from the url if available and
enters them in the relevant part of the new-links form of your Dew-NewPHPLinks
installation automagically. Currently it catches the following information and
meta-tags to your liking:
<ul>
<li>Site name</li>
<li>Site URL</li>
<li>description-meta tag</li>
<li>keywords-meta tag</li>
<li>author-meta tag</li>
<li>documents last modified-Date</li>
</ul>

additionally you can let the bookmarklet automagically enter the following
Dew-NewPHPLinks data:
<ul>
<li>User Name</li>
<li>password</li>
<li>password 2</li>
<li>Hint</li>
<li>Email</li>
</ul>
<br />
I had to make two versions of the bookmarklet because of a stupid limitation of IE
6.0 (IE < 6.0 did not have this limitation!). The IE version is crippled in
features. I recommend to use a different browser anyway!<br /><br />
For me this improves usefullness of Dew-NewPHPLinks +80% so I took the time to write it.
The use of meta tag information strangely is yet missing in all existing similar
bookmarklet applications to my knowledge.<br /><br />
What else can I do for you? Bookmarklets/Scriptlets there we go! (scriptlet ==
bookmarklet)<br />
This is a Bookmarklet as requested in: <a
href="http://www.dew-code.com/modules/newbb/viewtopic.php?topic_id=233&forum=5#forumpost822"
target="_blank">Dew-NewPHPLinks Forum</a><br /><br />
The code in these bookmarklets and in the php bookmarklet builder is open sourced
under the GNU-licence. Please send me a little notice if the code was of help for
one of your GNU open source projects- thank's.<br /><br />
</div>

<div class="<?php echo $c_mode1 ?>">
<h1>1 &nbsp;Preconditions</h1>
<ul>
<li>you are using a <a href="http://www.dew-code.com/modules/mydownloads/"
target="_blank">Dew-NewPHPLinks</a> database</li>
<li>java-script enabled in your browser (=standard setting)</li>
<li>access to the installed Dew-NewPHPLinks  server (1 file to edit) or contact admin</li>
</ul>
</div>

<div class="<?php echo $c_mode2 ?>">
<h1>2 &nbsp;Prepare Dew-NewPHPLinks (Code Change)</h1> 
<br />
Short explanation for the change (for non coders):<br />
Unluckily the current add.php script of &quot;Dew-NewPHPLinks&quot; can only cope with
POST variables. bookmarklets can only cope with GET variables.<br />
In future versions of Dew-NewPHPLinks this change will be already included!<br />

<h4>SIMPLE CODE CHANGE</h4><br />
in Dew-NewPHPLinks &quot;/include/add.php&quot; lines ~12-24<br />
starting with
&quot;$SiteName=mysql_escape_string($_POST[&#39;SiteName&#39;]);&quot;...<br />
ending with ...&quot;$submit_add=$_POST[&#39;submit_add&#39;];&quot;<br />
replace with:<br />
&quot;<br />
$SiteName=mysql_escape_string($_REQUEST[&#39;SiteName&#39;]);<br />
$SiteURL=mysql_escape_string($_REQUEST[&#39;SiteURL&#39;]);<br />
$RecipURL=mysql_escape_string($_REQUEST[&#39;RecipURL&#39;]);<br />
$Description=mysql_escape_string($_REQUEST[&#39;Description&#39;]);<br />
$Category=mysql_escape_string($_REQUEST[&#39;Category&#39;]);<br />
$Country=mysql_escape_string($_REQUEST[&#39;Country&#39;]);<br />
$UserName=mysql_escape_string($_REQUEST[&#39;UserName&#39;]);<br />
$PW=mysql_escape_string($_REQUEST[&#39;PW&#39;]);<br />
$PID=mysql_escape_string($_REQUEST[&#39;PID&#39;]);<br />
$PW2=mysql_escape_string($_REQUEST[&#39;PW2&#39;]);<br />
$Hint=mysql_escape_string($_REQUEST[&#39;Hint&#39;]);<br />
$Email=mysql_escape_string($_REQUEST[&#39;Email&#39;]);<br />
$submit_add=$_REQUEST[&#39;submit_add&#39;];<br />
&quot;<br /><br />
<h4>That&#39;s it! You can go to the next step-</h4>
<br /><br />
Short explanation for the change (for coders):<br />
So there has to be a minor change made in the code of add.php to make these
bookmarklets work because it is almost impossible to make bookmarklets that post
data via the POST method (except via XML HTTP request object = too complicated to
implement for this purpose).<br />
So all you have to do is to alter the code so the add.php script considers POST as
well as GET variables when they come correctly named. All I&#39;ve done ist to
replace the loading of the variables from &quot;$_POST[&#39;x&#39;]&quot; to
&quot;$_REQUEST[&#39;x&#39;]&quot;.<br /><br />
</div>

<div class="<?php echo $c_mode3 ?>">
<h1>3 &nbsp;Your Bookmarklet Settings</h1>
<form action="<?php echo $_SERVER['PHP_SELF'].'?r_mode=4';?>" method="post">
<h4>Main Settings (obligatory)</h4>
<strong>phpLinks Server Directory:</strong><br /><input type="text" size="65"
name="r_phplinksurl" value="<?php
if($v_phplinksurl==""):print($d_phplinksurl);else:echo $v_phplinksurl;endif;?>"><br
/>
(URL in the format: "http://www.some.so/phplinks"; <br />your server, <strong>NO</strong> trailing slash
please!)<?php echo $v_phplinksurl; ?><br /><br />
<strong>Just leave these empty if you don't (want) to use them:</strong><br />
<strong>phpLinks username:</strong><br /><input type="text" size="25"
name="r_UserName" value="<?php echo $v_UserName?>"> (your phplinks username)<br />
<strong>phpLinks e-mail:</strong><br /><input type="text" size="25" name="r_Email"
value="<?php echo $v_Email?>"> (your phplinks e-mail)<br />
<strong>phpLinks password 1:</strong><br /><input type="text" size="25" name="r_PW"
value="<?php echo $v_PW?>"> (your phplinks password 1)<br />
<strong>phpLinks password 2:</strong><br /><input type="text" size="25" name="r_PW2"
value="<?php echo $v_PW2?>"> (your phplinks password 2)<br />
<strong>phpLinks hint:</strong><br /><input type="text" size="25" name="r_Hint"
value="<?php echo $v_Hint?>"> (your phplinks hint)<br /><br />
<input class="submit" type="submit" name="enter" value="Build my Bookmarklet"><br />
<h4>Optional Settings</h4>
<input type="checkbox" name="r_description" value="TRUE"<?php
if($v_description=="")echo ' checked'; ?> /> Get Description from Web-Site<br /> 
Prefix Keywords: <input type="text" size="25" name="r_prefix_key" value="<?php echo
$v_prefix_key; ?>"><br />
<input type="checkbox" name="r_keywords" value="TRUE"<?php if($v_keywords=="")echo '
checked'; ?> /> Get Keywords from Web-Site<br /> 
Prefix Author: <input type="text" size="25" name="r_prefix_author" value="<?php echo
$v_prefix_author; ?>"><br />
<input type="checkbox" name="r_author" value="TRUE"<?php if($v_author=="")echo '
checked'; ?> /> Get Author from Web-Site<br /> 
Prefix Modified Date: <input type="text" size="25" name="r_prefix_modified"
value="<?php echo $v_prefix_modified; ?>"><br />
<input type="checkbox" name="r_lastModified" value="TRUE"<?php
if($v_lastModified=="")echo ' checked'; ?> /> Get last Modified Date from
Web-Site<br /> 

<h4>Popup Window Settings (optional)</h4>
<table>
<tr>
 <td>width: </td>
 <td><input type="text" size="4" name="r_width" value="<?php if($v_width==""):echo
$d_width;else:$v_width;endif;?>">px (<?php echo $d_width?> suggested)<br />
</td></tr>
<tr>
 <td>height: </td>
 <td><input type="text" size="4" name="r_height" value="<?php if($v_height==""):echo
$d_height;else:$v_height;endif;?>">px (<?php echo $d_height?> suggested)<br />
</td></tr>
<tr>
 <td>scrollbars: </td>
 <td><select name="r_scrollbars"><option label="yes" value="yes"<?php
if($v_scrollbars==""):print(" selected");endif;?>>yes</option><option label="no"
value="no"<?php
if($v_scrollbars=="no"):print("selected");endif;?>>no</option></select> (yes
suggested)<br />
</td></tr>
<tr>
 <td>resizable: </td>
 <td><select name="r_resizable"><option label="yes" value="yes"<?php
if($v_resizable==""):print(" selected");endif;?>>yes</option><option label="no"
value="no"<?php
if($v_resizable=="no"):print("selected");endif;?>>no</option></select> (yes
suggested)<br />
</td></tr>
</table>
<input type="hidden" size="4" name="r_mode" value="9"><br />
<input class="submit" type="submit" name="enter" value="Build my Bookmarklet">
<br /><br />
</div>
<br />&nbsp;<br />
<div class="<?php echo $c_mode3 ?>">
<h4>### IE 6.0x version ###</h4>
IE 6.0x only can take a maximum of 508 characters within a link/bookmarklet! All
prior version could cope with (so this version has reduced functionality and only
~481 chars) if you exceed the maximum number of chars after changing username,
e-mail and location then you have only the option to leave the user name and or
e-mail away and enter it manually every time you input a new url or to reduce some
of the remaining functionality (happy hacking!).<br /><br />
</div>

<div class="<?php echo $c_mode4 ?>">
<h1>4 &nbsp;Here come your bookmarklets</h1><br />
<a href="http://en.wikipedia.org/wiki/Drag-and-drop" target="self">"drag &amp;
drop"</a>
 the appropriate link into the bookmark-bar or onto the bookmark menue of your
browser. &nbsp;&nbsp;If there is a popup message/warning push the OK buttons-<br />
<h4>
<?php 
echo buildlinks(); 
if($r_mode==9 && ($v_phplinksurl !==$d_phplinksurl && $v_phplinksurl !==""))print
('<br />Congratulations!');
?>
</h4>
</div>

<div class="<?php
if($t_mode!=9||$v_phplinksurl==$d_phplinksurl||$v_phplinksurl==""):print
$defstate;else:echo $c_mode4;endif; ?>">
You now should have got a new bookmark(let) wherever you put it.<br />
Find out how it works by clicking it like you would with any bookmark and see what
happens -- See something happening?<br /><br />It will only work when java-script is
enabled in your browser (=standard setting).<br /><br /><br /><br />
code to use to display this bookmarklet on a web page:<br />
<textarea name="bookmarklethtml" rows="10" cols="60">
<?php echo buildlinks();?>
</textarea><br /><br />
<strong>Below you can see the settings you chose:</strong><br />
<?php $splitinfo=" &nbsp;"; ?>
phpLinks Server Directory:<?php echo $v_phplinksurl;?><br />
phpLinks username:<?php echo $v_UserName.$splitinfo;?>
phpLinks e-mail:<?php echo $v_Email.$splitinfo;?>
phpLinks password 1:<?php echo $v_PW.$splitinfo;?>
phpLinks password 2:<?php echo $v_PW2.$splitinfo;?>
phpLinks hint:<?php echo $v_Hint;?><br />
Web-Site Description:<?php print ($v_description.$splitinfo);?>
Keywords:<?php print ($v_keywords.$splitinfo);?>
Author:<?php print ($v_author.$splitinfo);?>
last Modified Date:<?php print ($v_lastModified);?><br /> 
width:<?php echo $v_width.$splitinfo;?> 
height:<?php echo $v_height.$splitinfo;?> 
scrollbars:<?php echo $v_scrollbars.$splitinfo;?> 
resizable:<?php echo $v_resizable;?><br /><br />
</div>

<div class="<?php echo $c_mode5 ?>">
<h1>5 &nbsp;Bookmarklet Usage</h1><br />
You now should have got a new bookmark(let) wherever you put it.<br />
Find out how it works by clicking it like you would with any bookmark and see what
happens -- See something happening?<br /><br />It will only work when java-script is
enabled in your browser (=standard setting).<br /><br /><br /><br />
code to use to display this bookmarklet on a web page:<br />

<h4>For more Information read This:</h4>
<a href="http://en.wikipedia.org/wiki/Bookmarklet"
target="_blank">http://en.wikipedia.org/wiki/Bookmarklet</a><br /><br /><br />
To further exercise, play around &amp; understand the use of Bookmarklets you might
go to:<br />
<a href="http://www.squarefree.com/bookmarklets/" target="_blank">Squarefree
Bookmarklets</a><br />
OR<br />
<a href="http://www.bookmarklets.com" target="_blank">bookmarklets.com</a><br />
<br /><br />
</div>

</div>
</body>
</html>



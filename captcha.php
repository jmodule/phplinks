<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 1/6/16
 * Time: 3:29 PM
 */

include("include/config.php");
include("include/functions.php");
include("include/common.php");

include("include/session.php");

// Adapted for The Art of Web: www.the-art-of-web.com
// Please acknowledge use of this code by including this header.

// initialise image with dimensions of 160 x 45 pixels
$image = @imagecreatetruecolor(160, 45) or die("Cannot Initialize new GD image stream");

// set background and allocate drawing colours
$background = imagecolorallocate($image, 0x66, 0xCC, 0xFF);
imagefill($image, 0, 0, $background);
$linecolor = imagecolorallocate($image, 0x33, 0x99, 0xCC);
$textcolor1 = imagecolorallocate($image, 0x00, 0x00, 0x00);
$textcolor2 = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);

// draw random lines on canvas
for($i=0; $i < 8; $i++) {
    imagesetthickness($image, rand(1,3));
    imageline($image, rand(0,160), 0, rand(0,160), 45, $linecolor);
}

// using a mixture of TTF fonts
$fonts = array();

// these default fonts disappeared from 1and1's server
//$fonts[] = "ttf-dejavu/DejaVuSerif-Bold";
//$fonts[] = "ttf-dejavu/DejaVuSans-Bold";
//$fonts[] = "ttf-dejavu/DejaVuSansMono-Bold";

// these fonts come from https://www.fontsquirrel.com/fonts/list/popular
$fonts[] = "./fonts/Allura-Regular.otf";
$fonts[] = "./fonts/GrandHotel-Regular.otf";
$fonts[] = "./fonts/KaushanScript-Regular.otf";

// add random digits to canvas using random black/white colour
$digit = '';
for($x = 10; $x <= 130; $x += 30) {
    $textcolor = (rand() % 2) ? $textcolor1 : $textcolor2;
    $digit .= ($num = rand(0, 9));
    imagettftext($image, 20, rand(-30,30), $x, rand(20, 42), $textcolor, $fonts[array_rand($fonts)], $num);
}

// record digits in session variable
$_SESSION['digit'] = $digit;

// display image and clean up
header('Content-type: image/png');
imagepng($image);
imagedestroy($image);

<div style="padding: 4px; width: 95%; margin: 0 auto;">
<?php
// *******************************************************************
// tags.php    revision 1
// *******************************************************************
//
// This code is based on an article in Dr. Dobb's Journal
// <www.ddj.com> by Jurgen Appelo called "Tag Clouds: Usability and
// Math".  It was adapted for use with phplinks.
//
// PHP Implementation by Jakim Friant <jakim@friant.org>
// Created: 2007-12-19

define("MAX_PER_LINE", 8);
define("EPSILON", 1.0e-8);
define("MIN_FONT", 4);
define("MAX_FONT", 36);
define("CAN_SORT", 1);
define("TRANSFORM", "bell"); //WARNING pareto curve is not finished

function getCategories()
{
  $cats = "";
  $query = sql_query("SELECT categories.Category AS cat, categories.Id as link, COUNT(links.id) AS cnt
                        FROM links, categories
                        WHERE links.Category=categories.Id
                        GROUP BY links.Category");
  while ( $row = sql_fetch_array($query) )
    {
      $cats[$row['cat']] = array('link' => $row['link'], 'cnt' => $row['cnt']);
    }
  return $cats;
}

function fromBellCurve($cats, $minSize, $maxSize)
{
  $weights = "";
  // First calculate the mean weight.
  $meansum = 0;
  foreach ($cats as $name => $data) {
    $meansum += $data['cnt'];
  }
  $mean = $meansum / count($cats);
  // Second, calculate the standard deviation of the weights.
  $sdsum = 0;
  foreach ($cats as $name => $data) {
    $sdsum += pow(($data['cnt'] - $mean), 2);
  }
  $sd = pow(((1 / count($cats)) * $sdsum), 0.5);
  // Now calculate the slope of a straight line from -2*sd to +2*sd.
  $slope = 0;
  if ($sd > 0) {
    $slope = ($maxSize - $minSize) / (4 * $sd);
  }
  // Get the value in the middle between minSize and maxSize.
  $middle = ($minSize + $maxSize) / 2;
  // Calculate the result for the given deviation from mean.
  foreach ($cats as $name => $data) {
    $result = 0;
    if ($sd = 0) {
      // With sd=0 all tags have the same weight
      $result = $middle;
    }
    else {
      // Calculate the distance from mean for this weight.
      $distance = $data['cnt'] - $mean;
      // Calcuatate the position on the slope for this distance.
      $result = ($slope * $distance + $middle);
      // If the tag turned out too small, set minSize.
      if ($result < $minSize) { $result = $minSize; }
      if ($result > $maxSize) { $result = $maxSize; }
    }
    $weights[$name] = array('link' => $data['link'], 'weight' => $result);
  }
  return $weights;
}

function fromParetoCurve($cats, $minSize, $maxSize)
{
  $min = -EPSILON;
  $max = EPSILON;
  // Convert each weight to it's log value.
  $logweights = "";
  foreach ($cats as $name => $data) {
    $w = log($data['cnt']);
    $logweights[$name] = array('link'=>$data['link'], 'weight'=>$w);
  }
  // Now calculate the slope of a straight line, from min to max.
  if ($max > $min) {
    $slope = ($maxSize - $minSize) / ($max - $min);
  }
    return 0;
}

$categories = getCategories();

if (TRANSFORM == "bell") {
  // transform them into a bell curve
  $weights = fromBellCurve($categories, MIN_FONT, MAX_FONT);
} else {
  $weights = fromParetoCurve($categories, MIN_FONT, MAX_FONT);
}

// display the list as links
$cnt = 1;
if ( $weights ) {
  if (CAN_SORT == 1) { ksort($weights); }
  foreach ( $weights as $category => $data ) {
    $link = "./directory/".$data['link'].".html";
    if (($cnt % MAX_PER_LINE) == 0) { print "</p>\n<p>"; }
    $size = floor($data['weight']);
    print "<a href=\"".$link."\" style=\"font-size: ".$size."px;\">".$category."</a> ";
    $cnt++;
  }
}
else { print "<b>Error: weights is blank</b>"; }
?>
</div>

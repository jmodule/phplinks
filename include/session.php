<?php
class MySessionHandler implements SessionHandlerInterface
{
    private $session_key = "id"; 
    private $session_value = "data"; 
    private $session_time = "expire"; 
    private $sdbh = null;
    private $expire =  300;
    
    function open($save_path, $session_name){ 
        global $dbhost, $dbuser, $dbpasswd, $dbname;
        $this->sdbh = mysqli_connect($dbhost, $dbuser, $dbpasswd);
        if (mysqli_connect_errno()){
            echo "<p>Database connection failed:</p>";
            echo "<p>" . mysqli_error($this->sdbh) . "</p>";
            exit;
        }
        $this->sdbh->select_db($dbname);
        return true; 
    } 
    function close(){ 
        return true; 
    } 
    function read($key){ 
        global $tb_sessions; 
        $query = " 
            select 
                {$this->session_value} 
            from 
                $tb_sessions 
            where 
                {$this->session_key} = '$key' 
            and 
                {$this->session_time} > UNIX_TIMESTAMP() 
        "; 
        if($result = mysqli_query($this->sdbh, $query)) {
            $record = mysqli_fetch_row($result);
            return $record[0];
        } else {
            return false; 
        } 
    } 
    function write($key, $val){ 
        global $tb_sessions, $expire; 
        $value = addslashes($val); 
        $query = " 
            replace into  
                $tb_sessions 
            values ( 
                '$key', 
                '$value', 
                UNIX_TIMESTAMP() + {$this->expire} 
            ) 
        ";
        if ( $this->sdbh != null) {
            $result = mysqli_query($this->sdbh, $query);
            if (! $result && $this->sdbh != null) {
                echo mysqli_error($this->sdbh);
            }
        } else {
            $result = null;
        }
        return $result;
    } 
    function destroy($key){ 
        global $tb_sessions; 
        $query = " 
            delete from 
                $tb_sessions 
            where 
                {$this->session_key} = '$key' 
        "; 
        $result = mysqli_query($this->sdbh, $query);
        return $result; 
    } 
    function gc($maxlifetime){ 
        global $tb_sessions; 
        $query = " 
            delete from 
                $tb_sessions 
            where 
                {$this->session_time} < UNIX_TIMESTAMP() 
        "; 
        $result = mysqli_query($this->sdbh, $query);
        return mysqli_affected_rows($this->sdbh);
    }
}
$handler = new MySessionHandler();
// FIXME: this database handler no longer works on friant.org
//session_set_save_handler($handler, true);
session_start();


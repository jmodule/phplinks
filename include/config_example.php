<?php
// *******************************************************************
//  include/config.php
// *******************************************************************

// *******************************************************************
//  MySQL Connection Information
//  leave $dbhost as is.  localhost will work on most virtual servers
//  change dbuser to your usr login
//  change dbpasswd to your login password
//  change dbname to your database name
//  All the above settings must be completed before installing PHPLinks
// *******************************************************************

// MySQL server hostname
	$dbhost		=	"localhost";

// MySQL server username
	$dbuser		=	"dbuser";

// MySQL server password
	$dbpasswd	=	"dbpasswd";

// MySQL database name
	$dbname		=	"dbname";

// *******************************************************************
//  phpLinks Language
//  choose your language here.
//
// *******************************************************************

	$language	=	"english";

// *******************************************************************
//  MySQL Table Names
// *******************************************************************

	$tb_categories		=	"phplinks_categories";
	$tb_links		    =	"phplinks_links";
	$tb_settings		=	"phplinks_settings";
	$tb_temp			=	"phplinks_temp";
	$tb_related	    	=	"phplinks_related";
	$tb_terms			=	"phplinks_terms";
	$tb_reviews	    	=	"phplinks_reviews";
	$tb_specs			=	"phplinks_specs";
	$tb_sessions		=	"phplinks_sessions";

// *******************************************************************
//	Other Settings
//	
// *******************************************************************

	// These are default values you can set to prefill the blanks
	// when you manually add a site from admin.  Can be blank as well.
	$prefill_email		=	"user@email.com";
	$prefill_username	=	"username";
	$prefill_password	=	"password";
	$prefill_hint		=	"hint";

?>

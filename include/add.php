<?php
// *******************************************************************
//  include/add.php
// *******************************************************************

$htmlsrc = $table2 . "<tr>\r\n\t<td class=\"whatText\">";
$htmlsrc .= $add_1 . "</td>\r\n</tr>\r\n</table>\r\n";

echo whattable("100%","center","",$htmlsrc);
unset($htmlsrc);

if (isset($_REQUEST['SiteName'])) { $SiteName = $_REQUEST['SiteName']; } else { $SiteName = ""; }
if (isset($_REQUEST['SiteURL'])) { $SiteURL = $_REQUEST['SiteURL']; } // leave unset
if (isset($_REQUEST['RecipURL'])) { $RecipURL = $_REQUEST['RecipURL']; } else { $RecipURL = ""; }
if (isset($_REQUEST['Description'])) { $Description = $_REQUEST['Description']; } else { $Description = ""; }
if (isset($_REQUEST['Category'])) { $Category = $_REQUEST['Category']; } // leave unset
if (isset($_REQUEST['Country'])) { $Country = $_REQUEST['Country']; } // leave unset
if (isset($_REQUEST['UserName'])) { $UserName = $_REQUEST['UserName']; } else { $UserName = ""; }
if (isset($_REQUEST['PW'])) { $PW = $_REQUEST['PW']; } else { $PW = ""; }
if (isset($_REQUEST['PID'])) { $PID = $_REQUEST['PID']; } else { $PID = ""; }
if (isset($_REQUEST['PW2'])) { $PW2 = $_REQUEST['PW2']; } else { $PW2 = ""; }
if (isset($_REQUEST['Hint'])) { $Hint = $_REQUEST['Hint']; } else { $Hint = ""; }
if (isset($_REQUEST['Email'])) { $Email = $_REQUEST['Email']; } else { $Email = ""; }

if (isset($_SESSION['digit'])) {
    // Save the digits for comparison later and remove it from the session so that it can't be reused.
	$last_session_digit = $_SESSION['digit'];
    $_SESSION['digit'] = "";
} else {
    $last_session_digit = "";
}

$htmlsrc = "";

// Handle form submission
if(isset($_REQUEST['submit_add'])){

////////////////////////////////////////////////////////////////////////////////////////
// PHPLinks Critical XSS Vulnerability Fix - By JeiAr - jeiar@kmfms.com //
///////////////////////////////////////////////////////////////////////////////////////
	$ip = $_SERVER['REMOTE_ADDR'];
	$info = $_SERVER['HTTP_USER_AGENT'];
	if (preg_match('/[-!#$%\'"*+\\.<->=?^_`{|}]$/', $SiteName)) {$err.= "Please enter A valid Site Name.<BR>";}
	if (preg_match('/[-!#$%&\'"*+.<->=?^_`{|}]$/', $SiteURL)) {$err.= "Please enter A valid Site URL.<BR>";}
	if (preg_match('/[-!#$%&\'"*+\\<->=?^_`{|}]$/', $Description)) {$err.= "Enter A valid Description.<BR>";}
	if (preg_match('/[-!#$%&\'"*+\\.<->=?^_`{|}]$/', $Category)) {$err.= "Enter A valid Category.<BR>";}
	if (preg_match('/[-!#$%&\'"*+\\.<->=?^_`{|}]$/', $Country)) {$err.= "Enter A valid Country.<BR>";}
	if (preg_match('/[-!#$%&\'"*+\\.<->=?^_`{|}]$/', $UserName)) {$err.= "Enter A valid UserName.<BR>";}
	if (preg_match('/[-!#$%&\'"*+\\.<->=?^_`{|}]$/', $PW)) {$err.= "Please enter A valid Password.<BR>";}
	if (preg_match('/[-!#$%&\'"*+\\.<->=?^_`{|}]$/', $PW2)) {$err.= "Please enter A valid Password.<BR>";}
	if (preg_match('/[-!#$%&\'"*+\\.<->=?^_`{|}]$/', $Hint)) {$err.= "Please enter A valid Hint.<BR>";}
	if (isset($err)) {
	echo $err;
	echo "<b>Possible Hack Attempt!!</b><br>";
	echo "<b>$ip</b><br>";
	echo "<b>$info</b><br>";
	echo "<a href=index.php?show=add>Back</a>";
	exit;
	}
///////////////////////////////////////////////////////////////////////////////////////////
	
	// Get current admin specs for adding new files
	$get_specs = sql_query("
		select
			*
		from
			$tb_specs
		where
			ID='1'
	");
	
	// Initialize $spec array
	$spec = sql_fetch_array($get_specs);
	
	unset($error);
	
	// Check for errors on submission
	if(strlen($SiteName) > $spec['SiteNameMax']){
		$error = 1;
		$site_name_error = 1;
	}

	if(strlen($SiteName) < $spec['SiteNameMin']){
		$error = 1;
		$site_name_error = 1;
	}

	if($SiteURL == "http://"){
		$SiteURL = "";
	}

	if($url_validate == "Y"){
		
		if(!$get_url = @fopen($SiteURL,"r")){
			
			$error = 1;
			$site_url_error = 1;
		}
	}

	$url = @parse_url($SiteURL);
	$concat_url = $url['host'];
    if (isset($url['path'])) {
        $concat_url .= $url['path'];
    }
	
	if(strlen($concat_url)>4){
		
		$check_url_links = sql_query("
			select
				*
			from
				$tb_links
			where
				SiteURL
			like
				'%$concat_url%'
		");

		if(sql_num_rows($check_url_links)>0){
			
			$error = 1; $duplicate_links_error = 1;
			$rows = sql_fetch_array($check_url_links);
			$ID = $rows['ID'];
		}

		$check_url_temp = sql_query("
			select
				*
			from
				$tb_temp
			where
				SiteURL
			like
				'%$concat_url%'
		");

		if(sql_num_rows($check_url_temp)>0){
		
			$error = 1;
			$duplicate_temp_error = 1;
		}

	}

	if(strlen($Description) > $spec['DescMax']){
		$error = 1;
		$description_error = 1;
	}

	if(strlen($Description) < $spec['DescMin']){
		$error = 1;
		$description_error = 1;
	}

	if(strlen($Country) == 0){
		$error = 1;
		$country_error = 1;
	}

	if(strlen($UserName) < $spec['UserNameMin']){
		$error = 1;
		$user_name_error = 1;
	}

	if(strlen($UserName) > $spec['UserNameMax']){
		$error = 1;
		$user_name_error = 1;
	}

	if(strlen($PW) < $spec['PWMin']){
		$error = 1;
		$password_error = 1;
	}

	if(strlen($PW) > $spec['PWMax']){
		$error = 1;
		$password_error = 1;
	}

	if($PW != $PW2){
		$error = 1;
		$password2_error = 1;
	}

	if(strlen($Hint) < $spec['HintMin']){
		$error = 1;
		$hint_error = 1;
	}

	if(strlen($Hint) > $spec['HintMax']){
		$error = 1;
		$hint_error = 1;
	}

	if(!preg_match("/" . $spec['EmailSpec'] . "/", $Email)){
		$error = 1;
		$email_error = 1;
	}

    if($_REQUEST['captcha'] != $last_session_digit) {
        $error = 1;
        $captcha_error = 1;
    }

	if(!isset($error)){

		if($validate == "N"){
			
			$insert_table = $tb_links;
		} else {
			
			$insert_table = $tb_temp;
		}

        $stmt = sql_prepare("
			insert into $insert_table(
				ID,
				SiteName,
				SiteURL,
				Added,
				Description,
				Category,
				Country,
				UserName,
				Password,
				Hint,
				Email
			) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        ");

        if ($stmt) {
            $entry_id = '';
            $entry_added = date('YmdHis', time());

            $PW_hashed = get_password_hash($PW);

            $stmt->bind_param("sssssssssss",
                $entry_id,
                $SiteName,
                $SiteURL,
                $entry_added,
                $Description,
                $Category,
                $Country,
                $UserName,
                $PW_hashed,
                $Hint,
                $Email
            );

            $result = $stmt->execute();

            if ($result) {
                if($validate == "N"){
                    $id = sql_insert_id();
                }

                $htmlsrc = $main_table . "<tr>\r\n\t";
                $htmlsrc .= "<td class=\"regularBoldText\">" . $add_2 . "</td>\r\n";
                $htmlsrc .= "</tr>\r\n";
                $htmlsrc .= "<tr>\r\n\t<td class=\"regularText\">" . $add_3;
                $htmlsrc .= stripslashes($SiteName) . "<br /><br />" . $add_4 . $SiteURL;
                $htmlsrc .= "<br /><br />" . $add_5 . stripslashes($Description);
                $htmlsrc .= "<br /><br />" . $add_6;

                $gc = sql_query("
                    select
                        Category
                    from
                        $tb_categories
                    where
                        ID='$Category'
                ");

                $fa = sql_fetch_array($gc);

                $Country = preg_replace("/[_]/"," ",$Country);
                $Country = preg_replace("/\.gif/i","",$Country);
                $Country = preg_replace("/\.png/i","",$Country);

                $htmlsrc .= $fa['Category'] . "<br /><br />" . $add_46 . $Country;
                $htmlsrc .= "<br /><br />" . $add_7 . $UserName;
                $htmlsrc .= "<br /><br />" . $add_8 . $Hint . "<br /><br />" . $add_9;
                $htmlsrc .= $Email . "<br /><br />";

                if($validate == "N"){

                    $sn = stripslashes($SiteName);

                    $htmlsrc .= $add_10 . "<form><textarea class=\"textBox\" ";
                    $htmlsrc .= "name=\"name\" rows=\"4\" cols=\"45\" readonly";
                    $htmlsrc .= "><a href=\"" . $base_url . "/in.php?ID=" . $id;
                    $htmlsrc .= "\" target=\"_blank\">" . $site_title . "</a><br /><br />";
                    $htmlsrc .= "</textarea></form>" . $add_11 . "<br /><br />";
                    $htmlsrc .= $add_12 . $sn . ".";
                    $htmlsrc .= "<form><textarea class=\"textBox\" name=\"name\" ";
                    $htmlsrc .= "rows=\"4\" cols=\"45\" readonly>";
                    $htmlsrc .= "<a href=\"" . $base_url . "/index.php?show=review_add&amp;SiteID=";
                    $htmlsrc .= $id . "\" target=\"_blank\">" . $add_13;
                    $htmlsrc .= $sn . "</a><br /><br /></textarea></form>";

                } else {

                    $htmlsrc .= $add_14;
                }

                $htmlsrc .= "<br /></td></tr></table>";

                if(($email_addition == "Y") && ($validate == "Y")){

                    include("include/email_addition.php");
                }
            } else {
                if ($stmt->errno > 0) {
                    $htmlsrc .= "<p><b>There was an error saving the record: " . $stmt->errno . "</b></p>";
                    $htmlsrc .= "<p>" . $stmt->error . "</p>";
                }
            }
            $stmt->close();
        }
	} else {
		$htmlsrc .= $table2 . "<tr>";
		$htmlsrc .= "<td class=\"errorTextBold\" align=\"center\">" . $add_15;
		$htmlsrc .= "</td></tr></table>";
	}
}

echo table("100%","center","",$htmlsrc);
unset($htmlsrc);

if(!isset($_REQUEST['submit_add']) || isset($error)){

	$htmlsrc = "<form method=\"post\" action=\"index.php?" . htmlspecialchars(SID) . "&amp;show=add\">" . $form_table . "<tr>";
	$htmlsrc .= "\r\n\t<td width=\"40%\" align=\"right\" valign=\"top\" ";
	$htmlsrc .= "class=\"regularText\">" . $add_16;
	$htmlsrc .= "</td>\r\n\t<td width=\"60%\" class=\"pagenav\">";
	$htmlsrc .= "<input class=\"textBox\" type=\"text\" name=\"SiteName\" ";
	$htmlsrc .= "maxlength=\"120\" size=\"35\" value=\"" . $SiteName . "\" />";

	if(isset($site_name_error)){

		$htmlsrc .= "<br /><span class=\"errorText\">" . $add_17 . $spec['SiteNameMin'];
		$htmlsrc .= $add_18 . $spec['SiteNameMax'] . $add_19 . ".</span>";
	}

	$htmlsrc .= "</td></tr><tr><td width=\"40%\" align=\"right\" valign=\"top\" ";
	$htmlsrc .= "class=\"regularText\">" . $add_20 . "</td><td width=\"60%\" ";
	$htmlsrc .= "class=\"pagenav\"><input class=\"textBox\" type=\"text\" name=\"SiteURL\" ";
	$htmlsrc .= "maxlength=\"100\" size=\"35\" value=\"";

	
	if(isset($SiteURL) && strlen($SiteURL)>0){		
		$htmlsrc .= $SiteURL;
	} else {	
		$htmlsrc .= "http://";
	}

	$htmlsrc .= "\" />";

	if(isset($site_url_error)){
		$htmlsrc .= "<br /><span class=\"errorText\">" . $add_21 . ".</span>";
	}

	if(isset($duplicate_links_error)){

		$htmlsrc .= "<br /><span class=\"errorText\">" . $add_22;
		$htmlsrc .= "<a href=\"index.php?" . htmlspecialchars(SID);
		$htmlsrc .= "&amp;show=update&amp;ID=" . $ID . "\">" . $add_23;
		$htmlsrc .= "</a>.</span>";
	}

	if(isset($duplicate_temp_error)){
		echo "<br /><span class=\"errorText\">" . $add_24 . ".</span>";
	}

	$htmlsrc .= "</td></tr><tr><td width=\"40%\" align=\"right\" valign=\"top\" ";
	$htmlsrc .= "class=\"regularText\">" . $add_25 . "</td><td width=\"60%\" ";
	$htmlsrc .= "class=\"pagenav\"><textarea class=\"textBox\" name=\"Description\" ";
	$htmlsrc .= "rows=\"7\" cols=\"40\">" . $Description;
	$htmlsrc .= "</textarea>";

	if(isset($description_error)){
		$htmlsrc .= "<br /><span class=\"errorText\">" . $add_26 . $spec['DescMin'];
		$htmlsrc .= $add_18 . $spec['DescMax'] . $add_28 . ".</span>";
	}

	$htmlsrc .= "</td></tr><tr><td width=\"40%\" align=\"right\" valign=\"top\" ";
	$htmlsrc .= "class=\"regularText\">" . $add_29 . "</td><td width=\"60%\" ";
	$htmlsrc .= "class=\"pagenav\"><select class=\"textBox\" name=\"Category\">";

	if(isset($Category)){
		$PID = $Category;
	}

	if(!isset($PID) && !isset($Category)){
		$PID = 0;
	}
	
	drop_cats($PID, 0, "", $cats);
	$htmlsrc .= $cats;

	$htmlsrc .= "</select></td></tr><tr><td width=\"40%\" align=\"right\" valign=\"top\" ";
	$htmlsrc .= "class=\"regularText\">" . $add_46 . "</td><td width=\"60%\" ";
	$htmlsrc .= "class=\"pagenav\"><select class=\"textBox\" name=\"Country\"><option ";
	$htmlsrc .= "value=\"\">" . $add_47 . "</option>";

	if(!isset($Country)){
		$Country = $default_country;
	}

	if($d = dir("./images/flags")){
		$htmlsrc .= getFlagList("./images/flags", $Country);
	}

	$htmlsrc .= "</select>";

	if(isset($country_error)){
		$htmlsrc .= "<br /><span class=\"errorText\">" . $add_48 . "</span>";
	}

	$htmlsrc .= "</td></tr><tr><td width=\"40%\" align=\"right\" valign=\"top\" ";
	$htmlsrc .= "class=\"regularText\">" . $add_30 . "</td><td width=\"60%\" ";
	$htmlsrc .= "class=\"pagenav\"><input class=\"textBox\" type=\"text\" ";
	$htmlsrc .= "name=\"UserName\" maxlength=\"16\" value=\"" . $UserName . "\" />";
	
	if(isset($user_name_error)){
		$htmlsrc .= "<br /><span class=\"errorText\">" . $add_31 . $spec['UserNameMin'];
		$htmlsrc .= $add_18 . $spec['UserNameMax'] . $add_33 . "</span>";
	}

	$htmlsrc .= "</td></tr><tr><td width=\"40%\" align=\"right\" valign=\"top\" ";
	$htmlsrc .= "class=\"regularText\">" . $add_34 . "</td><td width=\"60%\" ";
	$htmlsrc .= "class=\"pagenav\"><input class=\"textBox\" type=\"password\" ";
	$htmlsrc .= "name=\"PW\" maxlength=\"16\" value=\"" . $PW . "\" />";

	if(isset($password_error)){
		$htmlsrc .= "<br /><span class=\"errorText\">" . $add_35 . $spec['PWMin'];
		$htmlsrc .= $add_18 . $spec['PWMax'] . $add_37 . "</span>";
	}

	$htmlsrc .= "</td></tr><tr><td width=\"40%\" align=\"right\" valign=\"top\" ";
	$htmlsrc .= "class=\"regularText\">" . $add_38 . "</td><td width=\"60%\" ";
	$htmlsrc .= "class=\"pagenav\"><input class=\"textBox\" type=\"password\" ";
	$htmlsrc .= "name=\"PW2\" maxlength=\"16\" value=\"" . $PW2 . "\" />";

	if(isset($password2_error)){
		$htmlsrc .= "<br /><span class=\"errorText\">" . $add_39 . ".</span>";
	}

	$htmlsrc .= "</td></tr><tr><td width=\"40%\" align=\"right\" valign=\"top\" ";
	$htmlsrc .= "class=\"regularText\">" . $add_40 . "</td><td width=\"60%\" ";
	$htmlsrc .= "class=\"pagenav\"><input class=\"textBox\" type=\"text\" ";
	$htmlsrc .= "name=\"Hint\" maxlength=\"50\" value=\"" . $Hint . "\" />";

	if(isset($hint_error)){
		$htmlsrc .= "<br /><span class=\"errorText\">" . $add_41 . $spec['HintMin'];
		$htmlsrc .= $add_18 . $spec['HintMax'] . $add_42 . ".</span>";
	}

	$htmlsrc .= "</td></tr><tr><td width=\"40%\" align=\"right\" valign=\"top\" ";
	$htmlsrc .= "class=\"regularText\">" . $add_43 . "</td><td width=\"60%\" ";
	$htmlsrc .= "class=\"pagenav\"><input class=\"textBox\" type=\"text\" ";
	$htmlsrc .= "name=\"Email\" maxlength=\"50\" value=\"" . $Email . "\" />";

	if(isset($email_error)){
		$htmlsrc .= "<br /><span class=\"errorText\">" . $add_44 . ".</span>";
	}

	$htmlsrc .= "</td></tr>";
	$htmlsrc .= "<tr><td width='40%' align='right' valign='top' class='regularText'><img src=\"captcha.php?" . htmlspecialchars(SID) . "\" width=\"120\" height=\"30\" border=\"1\" alt=\"" . $add_49 . "\">";
	$htmlsrc .= "<td width='60%' class='pagenav'><input class='textBox' type=\"text\" size=\"6\" maxlength=\"5\" name=\"captcha\" value=\"\"><br />";
	$htmlsrc .= "<small class='regularText'>" . $add_50 . "</small>";

    if(isset($captcha_error)) {
	    $htmlsrc .= "<br /><span class='errorText'>" . $add_51 . ".</span>";
        $htmlsrc .= "<br /><span class='errorText'>You entered = " . $_REQUEST['captcha'] . ", Previous digits = " . $last_session_digit . "</span>";
    }
    $htmlsrc .= "</td></tr>";

	$htmlsrc .= "<tr><td width=\"100%\" colspan=\"2\" align=\"center\">";
	$htmlsrc .= "<br /><input type=\"submit\" name=\"submit_add\" value=\"" . $add_45;
	$htmlsrc .= "\" /></td></tr>";

	$htmlsrc .= "</table></form>";

	echo table("100%","center","",$htmlsrc);
	unset($htmlsrc);
}
